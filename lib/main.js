"use strict";

/* SugarCRM REST API module in node.js */

var request = require('request');
var https = require('https')


/* Global */

var apiURL = "";
var login = "";
var passwd = "";
var sessionID = "";


/* Config Initialisation */
var init = function (initArray) {
	apiURL = initArray.apiURL;
	login = initArray.login;
	passwd = initArray.passwd;
}
exports.init = init;


/* Show Config Info */
var configInfo = function (initArray) {

	return {
		apiURL: apiURL
		, login: login
		, passwd: passwd
	}
}
exports.configInfo = configInfo;


/* get a session ID */
exports.login = function (callback) {

	var subargs = {
		user_auth: {
			"user_name": login,
			"password": passwd,
			encryption: 'PLAIN'
		},
		application: "SugarCRM RestAPI Example"
	}

	var subargsInString = JSON.stringify(subargs);

	var data = {
		method: "login",
		input_type: "JSON",
		response_type: "JSON",
		rest_data: subargsInString
	};



	request(apiURL, { qs: data, agent: agent }, function (e, r, body) {
		// console.log(body)
		sessionID = JSON.parse(body).id;
		callback(sessionID);
	});
}

	var agentOptions = {
		host: 'localhost',
		path: '/',
		port: '443',
		rejectUnauthorized: false
	}

	var agent = new https.Agent(agentOptions)

/* pure POST call function */
var call = function (method, parameters, callback) {

	var data = {
		method: method,
		input_type: "JSON",
		response_type: "JSON",
		rest_data: JSON.stringify(parameters)
	};

	request.post(apiURL, { qs: data, agent: agent}, function (e, r, body) {

		try {
			var res = JSON.parse(body);
			callback(res, false);
		} catch (e) {
			callback(false, { error: body });
		}


	})
}
exports.call = call;














